// Copyright Epic Games, Inc. All Rights Reserved.

#include "JamGameMode.h"
#include "JamHUD.h"
#include "JamCharacter.h"
#include "UObject/ConstructorHelpers.h"

AJamGameMode::AJamGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AJamHUD::StaticClass();
}
